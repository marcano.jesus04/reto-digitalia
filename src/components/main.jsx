import React from "react";
import { Col, Row } from 'react-bootstrap';
import DefaultImg from '../img/icons/picture.png'
import UserImg from '../img/user/user.png'

function MainContent() {
    return(
        <React.Fragment>
            <div className="mx-5 mt-4">
                <h4>Bienvenido Bahuer. S.A.C</h4>
                <div className="create-publication mt-3 p-3 d-flex align-middle">
                  <img src={UserImg} height="60px" alt="user img" />
                  <h5 className='pt-3 px-4'>Crear Publicación</h5>
                </div>
                <div className="navigate-main-content d-flex mt-3 pb-3">
                  <div className="navigate-main-item pt-1"><p>Imagenes</p></div>
                  <div className="navigate-main-item pt-1"><p>????????</p></div>
                  <div className="navigate-main-item pt-1"><p>????????</p></div>
                </div>
                <div className="d-flex mt-3">
                  <div className="navigate-wall1 pt-1"><p>MURO 1</p></div>
                  <div className="navigate-wall2 pt-1"><p>MURO 2</p></div>
                </div>
                <div className="d-flex justify-content-around mt-4">
                  <select name="px-3" id="" className='button'>
                    <option value="#">Opcion1</option>
                  </select>
                  <select name="px-3" id="" className='button'>
                    <option value="#">Opcion1</option>
                  </select>
                  <select name="px-3" id="" className='button'>
                    <option value="#">Opcion1</option>
                  </select>
                  <select name="px-3" id="" className='button'>
                    <option value="#">Opcion1</option>
                  </select>
                  <button className='button'>
                    Aplicar
                  </button>
                </div>
                <div className="publications-container px-3 py-4 mt-4">
                    <div className="card p-3">
                        <div className="public-header d-flex pb-2">
                            <img src={UserImg} height="30px" width="30px" alt="Imagen de la Empresa" />
                            <div className="ps-2">
                                <h4>Empresa XYZ</h4>
                            </div>
                        </div>
                        <div className="public-content d-flex pb-2">
                            <Row>
                                <Col xl="8">
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatibus earum nulla tempore iusto expedita, cumque animi asperiores officia eum dolor.</p>
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Architecto deleniti quisquam, odio perferendis itaque ex asperiores accusamus beatae.</p>
                                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nisi, veniam.</p>
                                </Col>
                                <Col xl="4">
                                    <img src={DefaultImg} height="100%" width="100%" alt="" />
                                </Col>
                            </Row>
                        </div>
                    </div>

                    <div className="card p-3 mt-4">
                        <div className="public-header d-flex pb-2">
                            <img src={UserImg} height="30px" width="30px" alt="Imagen de la Empresa" />
                            <div className="ps-2">
                                <h4>Empresa XYZ</h4>
                            </div>
                        </div>
                        <div className="public-content d-flex pb-2">
                            <Row>
                                <Col xl="8">
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatibus earum nulla tempore iusto expedita, cumque animi asperiores officia eum dolor.</p>
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Architecto deleniti quisquam, odio perferendis itaque ex asperiores accusamus beatae.</p>
                                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nisi, veniam.</p>
                                </Col>
                                <Col xl="4">
                                    <img src={DefaultImg} height="100%" width="100%" alt="" />
                                </Col>
                            </Row>
                        </div>
                    </div>
                </div>

              </div>
        </React.Fragment>
    );
}

export default MainContent