import React from "react"; 
import Link from '../img/icons/link.png'
import UserImg from '../img/user/user.png'

function Sidebar() {

            return(
        <React.Fragment>
            <div className="sticky-top sidebar pt-4">
                <div className="section1 p-2 mx-4 d-flex justify-content-around">
                    <img src={Link} height="35px" alt="link img" />
                    <h5>Supply-Link</h5>
                </div>
                <div className="d-flex justify-content-center pt-4">
                        <figure>
                            <img src={UserImg} height="60px" alt="user img" />
                        </figure>
                </div>
                <div className="px-5">
                    <p>user info <br />
                    user info <br />
                    Lorem ipsum dolor sit amet consectetur</p>

                </div>
                <div className="border-t-b px-3 pb-2 py-3">
                    <p>CREAR REQUERIMIENTOS</p>
                    <ul>
                        <li>Requerimientos Materiales</li>
                        <li>Requerimiento de Servicios</li>
                    </ul>
                </div>
                <div className="px-3 pb-2 py-3">
                    <p>MIS REQUERIMIENTOS</p>
                    <ul>
                        <li>Requerimientos Materiales</li>
                        <li>Requerimiento de Servicios</li>
                    </ul>
                    
                </div>
            </div>
        </React.Fragment>
    );
}

export default Sidebar
