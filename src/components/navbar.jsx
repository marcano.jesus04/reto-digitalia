import React from "react";
import { CiSearch } from "react-icons/ci";
import { CiCirclePlus } from "react-icons/ci";
import UserImg from '../img/user/user.png'

function Navbar() {

    return(
        <React.Fragment>
            <div className="navbar d-flex justify-content-between py-3">
                
                    <div className="input-group-prepend d-flex w-50">
                        <span className="input-group-text"><CiSearch/></span>
                        <input type="text" className="form-control" aria-label="Amount (to the nearest dollar)"/>
                    </div>

                <div className="login d-flex me-5">
                    <div className="px-3 pt-1">
                            <CiCirclePlus/>
                    </div>                    
                    <div className="px-3 pt-1">
                            <CiCirclePlus/>
                    </div>                    
                    <div className="px-3 pt-1">
                            <CiCirclePlus/>
                    </div>
                    <img src={UserImg} height="40px" alt="user img" />
                </div>
            </div>
        </React.Fragment>
    );
}

export default Navbar