import React from "react";
import UserImg from '../img/user/user.png'

function Aside() {
	return(
		<React.Fragment>
			<div className="asidebar ps-5 pa-3 pt-5">
				<h5>Red de interés</h5>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>

				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div className="interest-container d-flex pt-3">
					<figure className="pt-2">
						<img src={UserImg} height="30px" alt="link img" />
					</figure>
					<div className="ps-2">
						<p>Lorem, ipsum dolor.
							Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
}

export default Aside