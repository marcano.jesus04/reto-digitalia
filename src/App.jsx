import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/navbar';
import Sidebar from './components/sidebar';
import MainContent from './components/main';
import Aside from './components/aside';
import { Col, Row } from 'react-bootstrap';

function App() {
  return (
    <div className="d-flex">
      <Row>
        <Col xl="2"><Sidebar /></Col>
        <Col xl="10" className="d-flex">
          <Row>
            <Col xl="12" className="sticky-top">
              <Navbar />
            </Col>
            <Col xl="9">
              <MainContent />
            </Col>
            <Col xl="3">
              <Aside />
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
}

export default App;
